---
---
layout: markdown_page
title: "PostgreSQL Upgrade - Design"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Resources

Epic: [gl-infra/106](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/106)

Blueprint: [PostgreSQL Upgrade Blueprint](https://about.gitlab.com/handbook/engineering/infrastructure/library/database/postgres/Postgresql-upgrade/blueprint/)

## Design

The objective of this design document is to describe the most important technical aspects of the PostgreSQL upgrade. Currently, our PostgreSQL cluster is at version 9.6. 

We are upgrading to version 11.7 due to business requirements.

The option chosen is to execute a pg_upgrade directly on the live cluster, the main reasons for this approach are:

The maintenance window has a duration of 2 hours. Considering this time constraint, and the time consumed to stop the platform and reinitialize the traffic, we need to focus on the main goal, which is the PostgreSQL upgrade. The goals of enabling checksums should be approached in a further iteration.

Considering the deadlines and the business requirements as: (the end of support of PostgreSQL version 9.x and 10.x)[https://gitlab.com/groups/gitlab-org/-/epics/2184] and supporting [the initiatives of database sharding](https://about.gitlab.com/company/team/structure/working-groups/database-sharding/) ) , the approach proposed is keep the project as simple and straightforward as possible. Besides, all the testing and interaction with all the necessary teams for this upgrade will demand time, concentration and effort from all involved. For more information about the discussed options and reasons consider the [PostgreSQL Upgrade Blueprint](https://about.gitlab.com/handbook/engineering/infrastructure/library/database/postgres/Postgresql-upgrade/blueprint/).


### Implementation Considerations

All the detailed steps of the change request that will test and apply this change in staging and production will be detailed in the migration plan. We will include all the tasks, pre-checks, post-checks, communications and tests, with the person responsible for each item. 
	
The upgrade process will consist of the following steps :

#### PG_UPGRADE 

Pg_upgrade : The following command will migrate the data between the major versions of PostgreSQL, in our case from 9.4 to 11.7. We will execute the pg_upgrade in all the nodes, during a maintenance window without traffic on the database cluster.
     

```pg_upgrade -b oldbindir -B newbindir -d oldconfigdir -D newconfigdir  -k```

The parameters meaning are: 

`-b oldbindir`: defines the path of the binaries from the old version. In our case the path of the binaries from version 9.4.

`-B newbindir`:  defines the path of the binaries from the new version. In our case the path of the binaries from version 11.7.

`-d oldconfigdir`: defines the path of the old configuration directory, that in our case would be the configuration folder from the version 9.6 

`-D newconfigdir`: defines the path of the new configuration directory. In the new configuration, we will have the optimized setup for PostgreSQL 11.7. The main tests we 
are executing is to enable the new features of query processing parallelism. 

`-K:` use hard links instead of copying files to the new cluster.


#### UPGRADING THE REPLICAS 

The standard PostgreSQL upgrade process, using pg_upgrade,  would require re-creating all the replicas. This would require a downtime of many hours. 

Executing the  pg_upgrade process in each node creates a new initdb, that would generate a different database identifier, then stream replication would not work.

The chosen approach to reduce the downtime will be execute the pg_upgrade into the following stages :

- Create the new database using `initdb` with the version (11.7). 
- rsync the new database files to all the nodes, keeping the same database identifier. In this step the files rsync are just the files related to the initdb, we do not rsync the datafiles from the database.
- Execute the pg_upgrade in all the nodes in parallel.
- Initialize the cluster

This process has been tested and all the results were successfull.

#### VACUUMDB ANALYZE-ONLY 

After executing the upgrade, during the downtime, It is necessary to gather statistics from the database on the newer version. Since the new database will be without statistics, to avoid performance issues with the incoming traffic. This task will be executed during the downtime with a high number of cores to be completed quickly.
	
```Vacuumdb -j 70  --analyze-only database_name```
	
The parameters meaning are: 

`-j number_of_jobs`: It will execute the task with the specified number of parallel jobs.

`--analyze-only`: Only calculate statistics for use by the optimizer (no vacuum executed).

#### VACUUMDB FREEZE	

After completing the analyze, we will start executing the VACUUM FREEZE ANALYZE.
Executing a VACUUM FREEZE helps to avoid any future vacuums triggered by txid wraparound protection. With this step we are reseting the variables from vacuum to do not carry them from the old cluster. During this execution we should start to restore the traffic.

```Vacuumdb -j 20 -F database_name```

The parameters meaning are: 

`-j number_of_jobs` : It will execute the task with the specified number of parallel jobs.

`-F`: Aggressively “freeze” tuples.  Specifying FREEZE is equivalent to performing VACUUM with the vacuum_freeze_min_age and vacuum_freeze_table_age parameters set to zero.


#### ROLLBACK

In the event of any unexpected problem, our rollback strategy is:


- Executing a snapshot from the data files when the database is in a consistent state, during the downtime when the database is down and all the data is flushed to the disk.

- We are not migrating all the nodes, we are sparing 4 nodes in the current version. In case of any issue, we could restore the environment in minimum time.
Promoting one of the replicas that are in version 9.6 to become a primary, and Patroni will resync the other 3 nodes to follow the new primary. The next step will be recreating the other nodes using the snapshot.

#### RISKS AND MITIGATIONS

The following is a list of risks and mitigations : 

- Exists the chance of having a degraded performance of some queries in the new version. We are benchmarking the new version with queries from our workload, and searching for possible optimizations that could be required for a good performance. But still, we could find some performance issues. The mitigation steps would be analyzing the possible problematic statements and make the changes needed to fix the situation.

- Data inconsistency. In the event of finding any data inconsistency during the tests after the upgrade process, the rollback process has to be executed.

- In the event of a rollback, we could be in a degraded performance until all the nodes would be recreated. 


------

Author: [Jose Finotto](https://gitlab.com/Finotto) {: .note}
